import React, { Fragment } from 'react';
import {Row, Col} from 'react-bootstrap'
class Header extends React.Component {
    constructor() {
        super()
        this.state = {}
    }
    render() {
        return (
            <Fragment >
                <Row className="bg-header mb-5 justify-content-md-center">
                    <Col md="auto">
                        <h1>To do List</h1>
                    </Col>
                </Row>
            </Fragment>
        );
    }
}

export default Header;