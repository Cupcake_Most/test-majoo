import React, { Fragment } from 'react';
import { Container, Table, Tabs, Tab } from 'react-bootstrap';
import { connect } from 'react-redux';
import { deleteToDO } from '../../State/Actions'
import Modal from './modal';
class Progress extends React.Component {
    constructor(props) {
        super(props);
        this.state = { 
            filter: 0 
        };
    }
    sortA;
    closeModal() {
		this.setState({
			isModalOpen: false
		});
	}
    handleChange(val) {
        let filter = val;
        this.setState({ filter });
    }
    toDoInProgress() {
        const self = this
        return this.props.todoList.filter(x => x.status === Number(this.state.filter)).sort(function (x, y) {
            // eslint-disable-next-line eqeqeq
            if (self.state.filter == 0) {
                return (y.createdAt < x.createdAt) ? 1 : -1;
            } else {
                return (x.createdAt < y.createdAt) ? 1 : -1;
            }
        });
    }
    render() {
        let tableTodo = this.toDoInProgress().map(function (x, key) {
            return <tr key={key}>
                <th>{x.title}</th>
                <th>{x.description}</th>
                <th>{x.status === 1 ? 'Done' : 'In Progress'}</th>
                <th>{x.createdAt}</th>
                <th>
                    <Modal title="View" isEdit={true} data={x}/>
                </th>
            </tr>
        });
        return (
            <Fragment >
                <Container>
                    <Tabs
                        activeKey={this.state.filter}
                        onSelect={(k) => this.handleChange(k)}
                        className="mb-3"
                    >
                        <Tab eventKey="0" title="In Progress">
                        </Tab>
                        <Tab eventKey="1" title="Done">
                        </Tab>
                    </Tabs>
                    <Modal title="Add" />
                    <Table striped bordered hover size="sm" className="mt-3">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Description</th>
                                <th>Status</th>
                                <th>Created At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            {tableTodo}
                        </tbody>
                    </Table>
                </Container>
            </Fragment>
        );
    }
}
const mapStateToProps = state => ({
    todoList: state.todoList
});
const mapDispatchToProps = dispatch => {
    return {
        hapusItem: (index) => {
            dispatch(deleteToDO(index))
        }
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(Progress);